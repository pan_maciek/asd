// Implementacja algorytmu MergeSort dla sortowania list. Scalanie iteratywne.

struct node {
  node *next;
  int val;
};

void split(node *source, node *&left, node *&right) {
  node *fast = source->next, *slow = source;
  // fast is moving 2 times faster, thus when fast is at the end slow is in the middle

  while (fast != nullptr) {
    fast = fast->next;
    if (fast != nullptr) {
      fast = fast->next;
      slow = slow->next;
    }
  }

  left = source;
  right = slow->next;
  slow->next = nullptr;
}

node *merge(node *left, node *right) {
  if (left == nullptr) return right;
  if (right == nullptr) return left;

  node *list;
  if (left->val < right->val) {
    list = left;
    left = left->next;
  } else {
    list = right;
    right = right->next;
  }
  node *prev = list;

  while (left != nullptr && right != nullptr) {
    if (left->val < right->val) {
      prev->next = left;
      left = left->next;
    } else {
      prev->next = right;
      right = right->next;
    }
    prev = prev->next;
  }

  prev->next = left != nullptr ? left : right;

  return list;
}

void sort(node *&root) {
  if (root == nullptr || root->next == nullptr) return;

  node *left, *right;

  split(root, left, right);

  sort(left);
  sort(right);

  root = merge(left, right);
}
