// Implementacja algorytmu MergeSort dla sortowania list. Scalanie rekurencyjne.

struct node {
  node *next;
  int val;
};

void split(node *source, node *&left, node *&right) {
  node *fast = source->next, *slow = source;
  // fast is moving 2 times faster, thus when fast is at the end slow is in the middle

  while (fast != nullptr) {
    fast = fast->next;
    if (fast != nullptr) {
      fast = fast->next;
      slow = slow->next;
    }
  }

  left = source;
  right = slow->next;
  slow->next = nullptr;
}

node *merge(node *left, node *right) {
  if (left == nullptr) return right;
  if (right == nullptr) return left;

  if (left->val < right->val) {
    left->next = merge(left->next, right);
    return left;
  } else {
    right->next = merge(left, right->next);
    return right;
  }
}

void sort(node *&root) {
  if (root == nullptr || root->next == nullptr) return;

  node *left, *right;

  split(root, left, right);

  sort(left);
  sort(right);

  root = merge(left, right);
}
