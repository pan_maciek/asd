## Proszę zaproponować/zaimplementować algorytm scalający k posortowanych tablic o łącznej długości n w jedną posortowaną tablicę w czasie O(n*log(k))

1. przygotowanie zmiennych  
  * tworzymy tablice długości n  
  * tworzymy kopiec min i wypełniamy go pierwszymi wartościami z tablic
2. dla każdego elementu i od 0 do n w tablicy docelowej  
  * przypisujemy wartość z wierzchołka kopca  
  * aktualizujemy wartość wierzchołka kopca następną wartością z   odpowiedniej tablicy, jeżeli tablica się już skończyła wpisujemy max_int  
  * naprawiamy kopiec w elemencie 1  
