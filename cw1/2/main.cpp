// Proszę zaproponować/zaimplementować algorytm scalający k posortowanych tablic o łącznej długości n w jedną posortowaną tablicę w czasie O(n*log(k))
#include <climits>

struct array_iterator { int value, *start, *end; };
struct array { int* values, length; };
struct heap { array_iterator* values; int size; };

void swap(array_iterator &a, array_iterator &b) {
	array_iterator c = a;
	a = b;
	b = c;	
}

int parent(int i) { return i / 2; }
int left(int i) { return i * 2; }
int right(int i) { return i * 2 + 1; }

void heapify(heap &h, int i) {
	int l = left(i), r = right(i), min = i;

	if (l <= h.size && h.values[l].value < h.values[min].value) min = l;
	if (r <= h.size && h.values[r].value < h.values[min].value) min = r;

	if (min != i) {
		swap(h.values[min], h.values[i]);
		heapify(h, min);
	}
}

void rebuild(heap &h) {
	for (int i = h.size / 2; i > 0; i--) 
		heapify(h, i);
}

heap create_heap(array* arrays, int k) {
	heap h;
	h.size = k;
	h.values = new array_iterator[k + 1];
	for (int i = 0; i < k; i++) {
		h.values[i + 1].value = arrays[i].values[0];
		h.values[i + 1].start = arrays[i].values;
		h.values[i + 1].end = arrays[i].values + arrays[i].length;
	}
	rebuild(h);
	return h;
}



int* merge(array* arrays, int k, int n) {
	int* out = new int[n];

	heap h = create_heap(arrays, k);

	for (int i = 0; i < n; i++) {
		out[i] = h.values[1].value;
		if (h.values[1].start == h.values[1].end - 1) h.values[1].value = INT_MAX;
		else h.values[1].value = *(++h.values[1].start);
		heapify(h, 1);
	}

	return out;
}