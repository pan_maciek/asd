#include "main.cpp"
#include <iostream>

void print(int *arr, int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << " ";
	}
}

int main() {

	array a, b, c;
	a.values = new int[5] { 7, 8, 9, 10, 11 };
	a.length = 5;

	b.values = new int[3] { 4, 5, 6 };
	b.length = 3;

	c.values = new int[4]{ 1, 2, 3, 12 };
	c.length = 4;

	array arrays[3] = { a, b, c };

	int* out = merge(arrays, 3, 12);

	print(out, 12);

}