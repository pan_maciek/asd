// Proszę zaproponować strukturę przechowującą liczby naturalne, w której operacje: Insert i GetMedian mają złożoność O(log(n))

// no exceptions implemented for push, pop and insert
#include <iostream>

#define size values[0]
struct heap {
  int *values, capacity;
};

heap create_heap(int capacity) {
  heap h;
  h.capacity = capacity + 1;
  h.values = new int[h.capacity];
  h.values[0] = 0;
  return h;
}

int parent(int i) { return i / 2; }
int left(int i) { return i * 2; }
int right(int i) { return i * 2 + 1; }

void swap(int &a, int &b) {
  int c = a;
  a = b;
  b = c;
}

void heapify_max(heap &h, int i) {
  int l = left(i), r = right(i), max = i;

  if (l <= h.size && h.values[l] > h.values[max]) max = l;
  if (r <= h.size && h.values[r] > h.values[max]) max = r;
  if (max != i) {
    swap(h.values[max], h.values[i]);
    heapify_max(h, max);
  }
}

void push_max(heap &h, int val) {
  h.values[++h.size] = val;

  for (int i = h.size, j; i > 1 && h.values[i] > h.values[j = parent(i)]; i = j)
    swap(h.values[i], h.values[j]);
}

int pop_max(heap &h) {
  int res = h.values[1];
  h.values[1] = h.values[h.size--];
  heapify_max(h, 1);
  return res;
}

void heapify_min(heap &h, int i) {
  int l = left(i), r = right(i), min = i;

  if (l <= h.size && h.values[l] < h.values[min]) min = l;
  if (r <= h.size && h.values[r] < h.values[min]) min = r;
  if (min != i) {
    swap(h.values[min], h.values[i]);
    heapify_min(h, min);
  }
}

void push_min(heap &h, int val) {
  h.values[++h.size] = val;

  for (int i = h.size, j; i > 1 && h.values[i] < h.values[j = parent(i)]; i = j)
    swap(h.values[i], h.values[j]);
}

int pop_min(heap &h) {
  int res = h.values[1];
  h.values[1] = h.values[h.size--];
  heapify_min(h, 1);
  return res;
}

struct median_heap {
  heap min, max;
};

median_heap create_median_heap(int capacity) {
  median_heap h;
  h.max = create_heap(capacity / 2 + 1);
  h.min = create_heap(capacity / 2 + 1);
  return h;
}

double get_median(const median_heap &h) {
  if (h.min.size > h.max.size) return h.min.values[1];
  if (h.min.size < h.max.size) return h.max.values[1];
  return (h.min.values[1] + h.max.values[1]) / 2.0;
}

void insert(median_heap &h, int val) {

  bool nead_rebalance = abs(h.min.size - h.max.size) > 1;

  if (val > get_median(h))
    push_min(h.min, val);
  else
    push_max(h.max, val);

  if (h.max.size - h.min.size > 1)
    push_min(h.min, pop_max(h.max));
  else if (h.min.size - h.max.size > 1)
    push_max(h.max, pop_min(h.min));
}

#undef size