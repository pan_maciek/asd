#include "./main.cpp"
#include <algorithm>
#include <iostream>
#include <time.h>
#include <vector>

double median(std::vector<int> v) {
  median_heap h = create_median_heap(v.size());
  for (int i = 0, len = v.size(); i < len; i++) {
    insert(h, v[i]);
  }
  return get_median(h);
}

double brut_median(std::vector<int> v) {
  std::sort(v.begin(), v.end());
  if (v.size() % 2 == 1) return v[v.size() / 2];
  return (v[v.size() / 2 - 1] + v[v.size() / 2]) / 2.0;
}

int main() {
  std::vector<std::vector<int>> tests = {
      {1},
      {1, 2, 3, 4},
      {1, 9, 2, 3, 5, 2},
      {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
      {80, 4, 13, 35, 1, 13, 80, 11, 67, 64, 60, 74, 72, 68, 43, 79, 92, 73, 61},
      {54, 64, 10, 22, 45, 84, 49, 74, 80, 17, 29, 14, 96, 46, 51, 73, 78, 44, 52},
      {58, 20, 51, 58, 26, 83, 64, 4, 80, 5, 99, 54, 88, 45, 20, 69, 51, 16, 36}};

  for (int i = 0; i < tests.size(); i++) {
    std::cout << "test #" << (i + 1) << std::endl;
    std::cout << "brut: " << brut_median(tests[i]) << std::endl;
    std::cout << "heap: " << median(tests[i]) << std::endl;
    std::cout << std::endl;
  }

  return 0;
}