int merge(int *arr, int *tmp, int left, int mid, int right) {
  int i = left, j = mid, k = left;
  int inv_count = 0;

  while ((i <= mid - 1) && (j <= right)) {
    if (arr[i] <= arr[j])
      tmp[k++] = arr[i++];
    else {
      tmp[k++] = arr[j++];
      inv_count = inv_count + (mid - i);
    }
  }

  while (i <= mid - 1)
    tmp[k++] = arr[i++];
  while (j <= right)
    tmp[k++] = arr[j++];
  for (i = left; i <= right; i++)
    arr[i] = tmp[i];

  return inv_count;
}

int merge_sort(int *arr, int *tmp, int left, int right) {
  int mid, count = 0;
  if (right > left) {
    mid = (right + left) / 2;
    count = merge_sort(arr, tmp, left, mid);
    count += merge_sort(arr, tmp, mid + 1, right);
    count += merge(arr, tmp, left, mid + 1, right);
  }
  return count;
}

int count_inversions(int *arr, int array_size) {
  int *temp = new int[array_size];
  return merge_sort(arr, temp, 0, array_size - 1);
}