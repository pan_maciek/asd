// Proszę zaimplementować algorytm QuickSort do sortowania listy jednokierunkowej.

struct node {
  node *next;
  int val;
};

void push_back(node **&end, node *n) {
  *end = n;
  end = &n->next;
}

void partition(node *root, node *&left, node *&right) {
  node **left_next = &left, **right_next = &right;
  node *iter = root->next;
  int pivot = root->val;

  while (iter != nullptr) {
    push_back(iter->val > pivot ? right_next : left_next, iter);
    iter = iter->next;
  }
  *right_next = *left_next = nullptr;
}

node **merge(node *&root, node *left, node **lend, node *right, node **rend) {
  if (left == nullptr) {
    root->next = right;
    return rend;
  }
  *lend = root;
  root->next = right;
  if (rend == nullptr)
    rend = &root->next;
  root = left;
  return rend;
}

node **quick_sort(node *&root) {
  if (root == nullptr)
    return nullptr;
  if (root->next == nullptr)
    return &root->next;

  node *left, *right;

  partition(root, left, right);
  node **lend = quick_sort(left);
  node **rend = quick_sort(right);
  return merge(root, left, lend, right, rend);
}
