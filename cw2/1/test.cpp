#include "./main.cpp"
#include <iostream>
#include <string>
#include <vector>

void prepend(node *&root, int val) {
  node *tmp = new node;
  tmp->next = root;
  tmp->val = val;
  root = tmp;
}

void printl(node *root, std::string label) {
  std::cout << label << ": ";
  while (root != nullptr) {
    std::cout << root->val << " ";
    root = root->next;
  }
  std::cout << std::endl;
}

node *list_from_vec(const std::vector<int> v) {
  node *list = nullptr;
  for (int i = v.size() - 1; i >= 0; i--)
    prepend(list, v[i]);
  return list;
}

int main() {

  std::vector<node *>list = {
      list_from_vec({1, 4, 2, 3, 5}),
      list_from_vec({29, 2, 83, 9, 0, 0, 84, 14, 98, 15, 20, 93, 61, 60, 19, 31, 98, 47, 67}),
      list_from_vec({86, 82, 56, 31, 68, 19, 20, 77, 12, 42, 1, 90, 53, 77, 73, 25, 91, 53, 53, 26})
  };

  for (int i = 0; i < list.size(); i++) {
    std::cout << "test #" << (i + 1) << std::endl;
    printl(list[i], "raw");
    quick_sort(list[i]);
    printl(list[i], "sorted");
    std::cout << std::endl;
  }
}
