// Proszę zaimplementować algorytm, który w czasie liniowym sortuje tablicę A zawierającą n liczb ze zbioru 0,...,n^2−1.

void swap(int *&a, int *&b) {
  int *c = a;
  a = b;
  b = c;
}

void copy_into(int *targer, int *source, int len) {
  for (int i = 0; i < len; i++)
    targer[i] = source[i];
}

// optimized for radix sort
void counting_sort(int *input, int *output, int len, int *count, int base, int offset) {
  int mask = base - 1;
  for (int i = 0; i < len; i++)
    count[(input[i] >> offset) & mask]++;
  for (int i = 1; i < base; i++)
    count[i] += count[i - 1];
  for (int i = len - 1; i >= 0; i--)
    output[--count[(input[i] >> offset) & mask]] = input[i];
}

void radix_sort(int *arr, int n) {
  int base = 1, offset_size = 0;
  while (base < n) { // base in power of 2 for efficiency
    base <<= 1;
    offset_size++;
  }

  int *count = new int[base], *tmp = new int[n], *original_arr = arr;
  int max = n * n - 1, prev = n * n; // dane w zadaniu normalnie trzeba znaleść
  for (int i = 0; i < base; i++) count[i] = 0;
  for (int offset = 0, i = 0; prev > (max >> offset); i++) {
    counting_sort(arr, tmp, n, count, base, offset);
    prev = max >> offset;
    offset += offset_size;
    swap(arr, tmp);
  }
  if (arr != original_arr) copy_into(original_arr, arr, n);
  delete[] count;
  delete[] tmp;
}