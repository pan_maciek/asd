#include "./main.cpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void printl(std::vector<int> v, std::string label) {
  std::cout << label << ":\t";
  for (int i = 0; i < v.size(); i++)
    std::cout << v[i] << ' ';
  std::cout << std::endl;
}

int _rand() { return std::rand() % 900; }

int main() {

  std::vector<int> v(30);
  std::generate(v.begin(), v.end(), _rand);
  std::vector<std::vector<int>> list = {
      {1},
      {2, 1},
      {1, 4, 2, 3, 5},
      {29, 2, 83, 9, 0, 0, 84, 14, 98, 15, 20, 93, 61, 60, 19, 31, 98, 47, 67},
      {86, 82, 56, 31, 68, 19, 20, 77, 12, 42, 1, 90, 53, 77, 73, 25, 91, 53, 53, 26},
      v};

  for (int i = 0; i < list.size(); i++) {
    std::cout << "test #" << (i + 1) << std::endl;
    printl(list[i], "raw");
    radix_sort(list[i].data(), list[i].size());
    printl(list[i], "sorted");
    std::cout << std::endl;
  }
}
