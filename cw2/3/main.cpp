// Mamy serię pojemników z wodą, połączonych (każdy z każdym) rurami.
// Pojemniki maja kształty prostokątów (2d), rury nie maja objętości (powierzchni).
// Każdy pojemnik opisany jest przez współrzędne lewego górnego rogu i prawego dolnego rogu.
// Wiemy, ze do pojemników nalano A wody (oczywiście woda rurami spłynęła do najniższych pojemników).
// Obliczyć ile pojemników zostało w pełni zalanych.

struct rect {
  int x_min, y_max, x_max, y_min;
};

inline int min(int a, int b) {
	return a > b ? b : a;
}

int water_needed_to_fill_h(rect *containers, int n, int h) {
	int water = 0;
	for (int i = 0; i < n; i++)
		water += (containers[i].x_max - containers[i].x_min) * (min(h, containers[i].y_max) - min(h, containers[i].y_min));
	return water;
}

void find_y_range(rect *containers, int n, int &y_min, int &y_max) {
	y_min = containers[0].y_min;
	y_max = containers[0].y_max;
	for (int i = 0; i < n; i++) {
		if (containers[i].y_min < y_min) y_min = containers[i].y_min;
		if (containers[i].y_max > y_max) y_max = containers[i].y_max;
	}
}

int count_below(rect* containers, int n, int h) {
	int count = 0;
	for (int i = 0; i < n; i++)
		if (containers[i].y_max <= h) count++;
	return count;
}

int count_flooded(rect *containers, int n, int water) {

	int y_min, y_max, y, needed, h;
	find_y_range(containers, n, y_min, y_max);
	y_max++;
	do {
		h = (y_max + y_min) / 2;
		needed = water_needed_to_fill_h(containers, n, h);
		if (needed <= water) y_min = h;
		else y_max = h;
	} while (y_max - y_min > 1);

	return count_below(containers, n, y_min);
}
