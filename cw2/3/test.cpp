#include "./main.cpp"
#include <iostream>
#include <vector>

int main() {
  std::vector<rect> v = {
    {0, 9, 2, 7},
    {3, 11, 5, 1},
    {6, 7, 10, 2},
    {6, 10, 10, 9},
    {11, 9, 14, 6},
    {15, 3, 16, 0}
  };

  std::cout << count_flooded(v.data(), v.size(), 52) << std::endl;
  std::cout << count_flooded(v.data(), v.size(), 11) << std::endl;
  std::cout << count_flooded(v.data(), v.size(), 1) << std::endl;
}