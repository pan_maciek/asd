// Dany jest ciąg przedziałów domkniętych [a1, b1], . . . ,[an, bn].
// Proszę zaproponować algorytm, który znajduje taki przedział [at, bt],
// w którym w całości zawiera się jak najwięcej innych przedziałów.

#include <algorithm>

struct pair {
  int y, i;
};

bool cmp(pair &a, pair &b) {
  return a.y < b.y;
}

pair find_max_range(pair *start, pair *end, int count) {
  int *offsets = new int[count];

  std::sort(start, start + count, cmp);
  std::sort(end, end + count, cmp);

  for (int i = 0; i < count; i++)
    offsets[start[i].i] = i;

  int max = -1, max_range_index;
  pair max_range;
  for (int i = 0; i < count; i++) {
    if (max < i - offsets[end[i].i]) {
      max = i - offsets[end[i].i];
      max_range_index = i;
    }
  }
  max_range.i = start[offsets[end[max_range_index].i]].y;
  max_range.y = end[max_range_index].y;
  return max_range;
}