#include "./main.cpp"
#include <iostream>

int main() {
  int n = 6;

  pair start[] = {{7}, {1}, {2}, {9}, {6}, {0}};
  pair end[] = {{1}, {11}, {7}, {10}, {9}, {3}};

  for (int i = 0; i < n; i++)
    start[i].i = end[i].i = i;

  pair r = find_max_range(start, end, n);
  std::cout << "max:\t" << r.i << " " << r.y << std::endl;

  return 0;
}