#include "./main.cpp"
#include <vector>
#include <iostream>

int main() {
  std::vector<string> test_data = {
      "lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit", "integer", "blandit",
      "sodales", "facilisis", "proin", "sed", "vulputate", "velit", "fusce", "sagittis", "tincidunt", "ipsum",
      "ornare", "dapibus", "mauris", "rhoncus", "eget", "aliquam", "erat", "volutpat", "vestibulum", "felis",
      "purus", "varius", "vitae", "varius", "sed", "tincidunt", "sed", "tellus", "maecenas", "at", "lobortis",
      "libero", "donec", "interdum", "nibh", "et", "libero", "sollicitudin", "pretium"};

    sort(test_data.data(), test_data.size(), 12);

    for (int i = 0; i < test_data.size();i++) {
      std::cout << test_data[i] << std::endl;
    }
}