// Jak sprawdzić czy podane słowa A i B są anagramami o długości n nad alfabetem k w czasie O(n) nie O(n + k)
#include <string>

using std::string;

bool are_anagrams(string A, string B, int n) {
  const int k = 256;
  static int count[k];

  for (int i = 0; i < n; i++) {
    count[A[i]]++;
    count[B[i]]--;
  }

  int i = 0;
  bool flag = true;
  for (; i < n; i++) {
    if (count[A[i]] != 0 || count[B[i]] != 0) {
      flag = false;
      break;
    }
    count[A[i]] = 0;
    count[B[i]] = 0;
  }

  for (; i < n; i++) {
    count[A[i]] = 0;
    count[B[i]] = 0;
  }
  return flag;
}