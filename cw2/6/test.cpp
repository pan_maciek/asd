#include "./main.cpp"
#include <iostream>
#include <vector>

int main() {
  std::vector<string> list = {
      "ala", "ala",
      "aneta", "antena",
      "bob", "alice",
      "kajak", "jakak"};

  for (int i = 0; i < list.size(); i += 2) {
    std::cout << "test #" << (i / 2 + 1) << std::endl;
    std::cout << list[i] << "\t" << list[i + 1] << "\t"
              << (are_anagrams(list[i], list[i + 1], list[i].size()) ? "true" : "fakse")
              << std::endl;
    std::cout << std::endl;
  }
}