// Dana jest posortowana tablica int A[N] oraz liczba x. Napisac program, który stwierdza czy istnieją indeksy i oraz j, takie ze A[i]+A[j]=x
// (powinno działać w czasie O(N))

struct tuple { 
  int i, j;
};

tuple find_indexes(int* A, int N, int x) {
  int i = 0, j = N - 1;
  while (i < j) {
    int tmp = A[i] + A[j];
    if (tmp > x) j--;
    else if (tmp < x) i++;
    else return { i, j };
  }
  return { -1, -1 };
}