#include "./main.cpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void printl(const std::vector<int> &a) {
  for (int i = 0, size = a.size(); i < size; i++)
    std::cout << a[i] << ' ';
  std::cout << std::endl;
}

int random_sum(const std::vector<int> &a) {
  int i = std::rand() % a.size(), j;
  do {
    j = std::rand() % a.size();
  } while (i == j);
  return a[i] + a[j];
}

int main() {

  std::vector<std::vector<int>> list = {
      {1, 2, 2, 3, 2},
      {29, 2, 83, 9, 0, 0, 84, 14, 98, 15, 20, 93, 61, 60, 19, 31, 98, 47, 67},
      {86, 82, 56, 31, 68, 19, 20, 77, 12, 42, 1, 90, 53, 77, 73, 25, 91, 53, 53, 26},
      {2, 2, 3, 3},
      {2, 2, 3, 3, 1},
      {2, 2, 3, 3, 2},
      {126, 2, 116, 2, 69, 2, 33, 2, 180, 2, 178, 2, 188, 2, 118, 2, 50, 2, 77, 2, 215, 2, 163, 2, 187, 2, 216, 2, 165, 2, 184, 2, 184, 2, 44, 2, 93, 2, 112, 2, 132, 2, 152, 2, 64, 2, 194, 2, 33, 2, 202, 2, 83, 2, 190, 2, 139, 2, 119, 2, 125, 2, 216, 2, 184, 2, 193, 2, 163, 2, 103, 2, 219, 2, 189, 2, 94, 2, 59, 2, 23, 2, 46, 2, 180, 2, 184, 2, 65, 2, 140, 2, 216, 2, 20, 2, 108, 2, 29, 2, 139, 2, 52, 2, 154, 2, 204, 2, 116, 2, 160, 2, 139, 2, 33, 2, 94, 2, 202, 2, 22, 2, 65, 2, 195, 2, 82, 2, 27, 2, 212, 2, 111, 2, 46, 2, 180, 2, 72, 2, 175, 2, 135, 2, 90, 2, 79, 2, 62, 2, 25, 2, 126, 2, 126, 2, 152, 2, 216, 2, 117, 2, 218, 2, 50, 2, 73, 2, 146, 2, 28, 2, 36, 2, 50, 2, 192, 2, 182, 2, 193, 2, 208, 2, 140, 2, 152, 2, 192, 2, 120, 2, 180, 2, 161, 2, 199, 2, 79, 2, 2},
      {2, 1, 2, 3, 2, 4, 2}};

  for (int i = 0, res; i < list.size(); i++) {
    std::cout << "test #" << (i + 1) << std::endl;
    std::sort(list[i].begin(), list[i].end());
    printl(list[i]);
    int x = random_sum(list[i]);
    tuple t = find_indexes(list[i].data(), list[i].size(), x);
    
    if (t.i != -1) std::cout << list[i][t.i] << " + " << list[i][t.j] << " = " << x << std::endl;
    else std::cout << "? + ? = " << x << std::endl;

    t = find_indexes(list[i].data(), list[i].size(), 100);
    if (t.i != -1) std::cout << list[i][t.i] << " + " << list[i][t.j] << " = " << 100 << std::endl;
    else std::cout << "? + ? = " << 100 << std::endl;
    std::cout << std::endl;
  }
}