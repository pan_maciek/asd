// Zaimplementować algorytm, który dla tablicy int A[N] wyznacza rekurencyjną medianę median. (magiczne piątki)

#include <algorithm>

void swap(int *a, int *b) {
  int c = *a;
  *a = *b;
  *b = c;
}

int *median_helper(int *s, int *e) {
  if (e - s <= 5) {
    std::sort(s, e);
    return s + (e - s) / 2;
  }

  for (int i = 0, k = (e - s) / 5; i < k; i++)
    swap(s + i, median_helper(s + i * 5, std::min(e, s + i * 5 + 5)));

  return median_helper(s, s + (e - s) / 5);
}

int median_of_medians(int *t, int n) {
  return *median_helper(t, t + n);
}

void partition(int *t, int n, int median, int &less, int &eq) {
  int l = 0, r = n - 1;
  for (int i = 0; i <= r; i++) {
    if (t[i] < median)
      std::swap(t[i], t[l++]);
    else if (t[i] > median)
      std::swap(t[i--], t[r--]);
  }
  less = l;
  eq = r - less + 1;
}

int select(int *t, int n, int k) {
  if (n <= 5) {
    std::sort(t, t + n);
    return t[k];
  }
  int m = median_of_medians(t, n), less, eq;
  partition(t, n, m, less, eq);
  if (less > k) return select(t, less, k);
  if (less + eq > k) return m;
  return select(t + less + eq, n - less - eq, k - less - eq);
}
