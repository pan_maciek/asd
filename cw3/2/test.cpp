#include "./main.cpp"
#include <iostream>
#include <vector>

void printl(const std::vector<int> &a) {
	for (int i = 0, size = a.size(); i < size; i++)
		std::cout << a[i] << ' ';
	std::cout << std::endl;
}

int main() {
	std::vector<std::vector<int>> list{
	  {138, 2, 106, 2, 187, 2, 59, 2, 140, 2, 208, 2, 24, 2, 49, 2, 95, 2, 118, 2},
	  {5, 9, 55, 20, 100, 1, 4, 21, 15, 82, 59, 12, 2019, 13, 3, 28, 10, 48, 72, 108, 20},
	  {1, 2, 3, 4, 5, 6, 7},
	  {65, 103, 110, 105, 101, 115, 122, 107, 97, 32, 68, 117, 116, 107, 97, 32, 60, 51},
	  {77, 97, 99, 105, 101, 106, 32, 75, 111, 122, 105, 101, 106, 97}
	};

	for (int i = 0; i < list.size(); i++) {
		int median_median = median_of_medians(list[i].data(), list[i].size());
		int median = select(list[i].data(), list[i].size(), list[i].size() / 2);
		std::sort(list[i].begin(), list[i].end());
		printl(list[i]);
		std::cout << "median of medians: " << median_median << std::endl;
		std::cout << "median:\t\t   " << median << std::endl;
		std::cout << "select(t, n, 2):   " << select(list[i].data(), list[i].size(), 2) << std::endl;
		std::cout << std::endl;
	}
}