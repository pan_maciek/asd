// Mamy daną tablicę A z n liczbami. Proszę zaproponować algorytm o złożoności O(n), który stwierdza, czy w tablicy ponad połowa elementów ma jednakową wartość.

bool half(int *A, int n, int &res) {
  int a, count = 0, i;

  for (i = 0; i < n; i++) {
    if (count == 0) a = A[i];
    count += A[i] == a ? 1 : -1;
  }

  if (count <= 0) return false;

  for (i = 0, count = 0; i < n; i++)
    if (A[i] == a) count++;

  return res = a, count > n / 2;
}
