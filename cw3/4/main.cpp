// Proszę zaproponować algorytm sortujący ciąg słów o różnych długościach w czasie proporcjonalnym do sumy długości tych słów?

#include <string>

using std::string;

void copy_into(string *targer, string *source, int len) {
  for (int i = 0; i < len; i++)
    targer[i] = source[i];
}

void counting_sort(string *input, string *output, int len, int *count, int base, int offset) {
  for (int i = 0; i < len; i++)
    count[input[i].size() > offset ? input[i][offset] : 0]++;
  for (int i = 1; i < base; i++)
    count[i] += count[i - 1];
  for (int i = len - 1; i >= 0; i--)
    output[--count[input[i].size() > offset ? input[i][offset] : 0]] = input[i];
}

void sort(string *arr, int n, int max_len) {
  int base = 256, *count = new int[base];
  string *tmp = new string[n], *original_arr = arr;
  for (int i = 0; i < base; i++) count[i] = 0;
  for (int offset = max_len - 1; offset >= 0; offset--) {
    counting_sort(arr, tmp, n, count, base, offset);
    swap(arr, tmp);
  }
  if (arr != original_arr) copy_into(original_arr, arr, n);
  delete[] count;
  delete[] tmp;
}
