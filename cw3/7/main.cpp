// Stworzyć jak najszybszy algorytm, który dla ciągu liter a, b oraz liczby k
// szuka najczęściej występujący spójny podciąg długości k
// * podciągi mogą się pokrywać

#include <stdlib.h>

char *hash_to_str(int hash, int k) {
  char *str = new char[k + 1];
  str[k] = '\0';
  for (int i = k - 1; i >= 0; hash >>= 1, i--)
    str[i] = 'a' + (hash & 1);
  return str;
}

char *find_max_occurring(const char *t, int n, int k) {
  int *count = (int *)calloc(1 << k, sizeof(int)), i;
  int hash = 0, mask = (1 << k) - 1;
  int max = 1, max_val;

  for (i = 0; i < k; i++)
    hash = hash << 1 | t[i] - 'a';

  count[max_val = hash];

  for (; i < n; i++)
    if (++count[hash = (hash << 1 | t[i] - 'a') & mask] > max)
      max = count[hash], max_val = hash;

  return hash_to_str(max_val, k);
}